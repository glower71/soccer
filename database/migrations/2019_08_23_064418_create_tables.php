<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->timestamps();
        });

        Schema::create('tournaments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->tinyInteger('is_finished')->default(0);
            $table->timestamps();
        });

        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tournament_id')->unsigned()->nullable();
            $table->bigInteger('team1_id')->unsigned()->nullable();
            $table->bigInteger('team2_id')->unsigned()->nullable();
            $table->bigInteger('score1')->unsigned()->nullable();
            $table->bigInteger('score2')->unsigned()->nullable();
            $table->tinyInteger('is_finished')->default(0);
            $table->timestamps();

            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
        });

        Schema::create('stages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tournament_id')->unsigned()->nullable();
            $table->unsignedBigInteger('match_id')->unsigned()->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('child1_id')->nullable();
            $table->unsignedBigInteger('child2_id')->nullable();
            $table->timestamps();

            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');
            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('stages')->onDelete('cascade');
            $table->foreign('child1_id')->references('id')->on('stages')->onDelete('set null');
            $table->foreign('child2_id')->references('id')->on('stages')->onDelete('set null');
        });

        Schema::create('match_teams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tournament_id')->unsigned()->nullable();
            $table->bigInteger('match_id')->unsigned()->nullable();
            $table->bigInteger('team_id')->unsigned()->nullable();
            $table->tinyInteger('goals_scored')->nullable();
            $table->tinyInteger('goals_passed')->nullable();
            $table->tinyInteger('is_win')->nullable();
            $table->tinyInteger('is_loss')->nullable();
            $table->tinyInteger('is_finished')->nullable();
            $table->tinyInteger('position')->nullable();
            $table->timestamps();

            $table->foreign('match_id')->references('id')->on('matches')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
        Schema::dropIfExists('tournaments');
        Schema::dropIfExists('stages');
        Schema::dropIfExists('matches');
        Schema::dropIfExists('match_teams');
    }
}
