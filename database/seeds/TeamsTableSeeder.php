<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
                ['name' => 'Россия'],
                ['name' => 'Египет'],
                ['name' => 'Уругвай'],
                ['name' => 'Саудовская Аравия'],
                ['name' => 'Иран'],
                ['name' => 'Португалия'],
                ['name' => 'Марокко'],
                ['name' => 'Франция'],
                ['name' => 'Дания'],
                ['name' => 'Австралия'],
                ['name' => 'Хорватия'],
                ['name' => 'Исландия'],
                ['name' => 'Аргентина'],
                ['name' => 'Нигерия'],
                ['name' => 'Швейцария'],
                ['name' => 'Бразилия'],
                ['name' => 'Коста-Рика'],
                ['name' => 'Германия'],
                ['name' => 'Швеция'],
                ['name' => 'Южная Корея'],
                ['name' => 'Англия'],
                ['name' => 'Бельгия'],
                ['name' => 'Тунис'],
                ['name' => 'Панама'],
                ['name' => 'Польша'],
                ['name' => 'Япония'],
                ['name' => 'Сенегал']
            ]
        );
    }
}
