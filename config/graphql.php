<?php

use App\GraphQL\Type\MatchTeamType;
use App\GraphQL\Type\MatchType;
use App\GraphQL\Type\StageType;
use App\GraphQL\Type\StatAggregationGroupType;
use App\GraphQL\Type\StatAggregationType;
use App\GraphQL\Type\StatAggregationValueType;
use App\GraphQL\Type\TeamType;
use App\GraphQL\Type\TournamentType;

return [

    // The prefix for routes
    'prefix' => 'graphql',

    // The routes to make GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Route
    //
    // Example:
    //
    // Same route for both query and mutation
    //
    // 'routes' => 'path/to/query/{graphql_schema?}',
    //
    // or define each routes
    //
    // 'routes' => [
    //     'query' => 'query/{graphql_schema?}',
    //     'mutation' => 'mutation/{graphql_schema?}'
    // ]
    //
    // you can also disable routes by setting routes to null
    //
    // 'routes' => null,
    //
    'routes' => '{graphql_schema?}',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\Rebing\GraphQL\GraphQLController@query',
    //     'mutation' => '\Rebing\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => '\Rebing\GraphQL\GraphQLController@query',

    // Any middleware for the graphql route groupStatAggregateQuery
    'middleware' => [],

    // The name of the default schema used when no argument is provided
    // to GraphQL::schema() or when the route is used without the graphql_schema
    // parameter.
    'schema' => 'default',

    // The schemas for query and/or mutation. It expects an array to provide
    // both the 'query' fields and the 'mutation' fields. You can also
    // provide directly an object GraphQL\Schema
    //
    // Example:
    //
    // 'schemas' => [
    //     'default' => new Schema($config)
    // ]
    //
    // or
    //
    // 'schemas' => [
    //     'default' => [
    //         'query' => [
    //              'users' => 'App\GraphQL\Query\UsersQuery'
    //          ],
    //          'mutation' => [
    //
    //          ]
    //     ]
    // ]
    //
    'schemas' => [
        'grid' => [
            'query' => [
                'teams' => 'App\GraphQL\Query\TeamsQuery',
                'stages' => 'App\GraphQL\Query\StagesQuery',
                'tournaments' => 'App\GraphQL\Query\TournamentsQuery',
                'tournament' => 'App\GraphQL\Query\TournamentQuery',
                'match' => 'App\GraphQL\Query\MatchQuery',
                'statAggregation' => 'App\GraphQL\Query\StatAggregationQuery',
            ],
            'mutation' => [
                'createTeam' => 'App\GraphQL\Query\CreateTeamMutation',
                'updateStage' => 'App\GraphQL\Query\UpdateStageMutation',
                'deleteStage' => 'App\GraphQL\Query\DeleteStageMutation',
                'createTournament' => 'App\GraphQL\Query\CreateTournamentMutation',
                'updateTournament' => 'App\GraphQL\Query\UpdateTournamentMutation',
                'deleteTournament' => 'App\GraphQL\Query\DeleteTournamentMutation',
                'emulateTournament' => 'App\GraphQL\Query\EmulateTournamentMutation',
                'addStageChild' => 'App\GraphQL\Query\AddStageChildMutation',
                'setMatchTeam' => 'App\GraphQL\Query\SetMatchTeamMutation',
                'setMatchScore' => 'App\GraphQL\Query\SetMatchScoreMutation',
            ]
        ],
    ],

    // The types available in the application. You can then access it from the
    // facade like this: GraphQL::type('user')
    //
    // Example:
    //
    // 'types' => [
    //     'user' => 'App\GraphQL\Type\UserType'
    // ]
    //
    // or whitout specifying a key (it will use the ->name property of your type)
    //
    // 'types' => [
    //     'App\GraphQL\Type\UserType'
    // ]
    //
    'types' => [
        'team' => TeamType::class,
        'stage' => StageType::class,
        'tournament' => TournamentType::class,
        'match' => MatchType::class,
        'matchTeam' => MatchTeamType::class,
        'statAggregation' => StatAggregationType::class,
        'statAggregationGroup' => StatAggregationGroupType::class,
        'statAggregationValue' => StatAggregationValueType::class,
    ],

    // This callable will received every Error objects for each errors GraphQL catch.
    // The method should return an array representing the error.
    //
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    //
    'error_formatter' => ['\Rebing\GraphQL\GraphQL', 'formatError']

];
