import {gMatch} from '../const.js';
import Query from 'graphql-query-builder-v2';

const state = {
    stages: {},
};

const getters = {};

// actions
const actions = {
    getStages({dispatch, commit}, payload = {}) {
        let query = new Query('stages').filter({tournament_id: payload.id}).find([
                'id',
                {
                    parent: ['id']
                }, {
                    child1: ['id']
                }, {
                    child2: ['id']
                }, gMatch,
                'depth_level'
            ]
        );

        return dispatch('requestGraphql', {type: 'query', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('setStages', {stages: _.keyBy(data.stages, 'id')});
                }
            });
    },
    addStage({dispatch, commit}, payload = {}) {
        let query = new Query('addStageChild', 'stage').find([
                'id',
                {
                    parent: ['id']
                }, {
                    child1: ['id']
                }, {
                    child2: ['id']
                }, gMatch,
                'depth_level'
            ]
        );

        query.filter({
            parent_id: payload.parent_id,
            position: payload.position
        });

        return dispatch('requestGraphql', {type: 'mutation', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('setStage', {stage: data.stage, position: payload.position});
                }
            });
    },
    deleteStage({dispatch, commit}, payload = {}) {
        let query = new Query('deleteStage', 'stage').filter({id: payload.id}).find(['id', {parent: ['id']}]);
        return dispatch('requestGraphql', {type: 'mutation', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('deleteStage', {
                        id: data.stage.id,
                        parent_id: payload.parent_id,
                        position: payload.position
                    });
                }
            });
    },
    setMatchTeam({dispatch, commit}, payload = {}) {
        let query = new Query('setMatchTeam', 'match').filter({
            match_id: payload.match_id,
            team_id: payload.team_id,
            position: payload.position
        }).find([
            'id',
            {team1: ['id', 'name']},
            {team2: ['id', 'name']},
            'score1',
            'score2'
        ]);

        return dispatch('requestGraphql', {type: 'mutation', query: query}, {root: true})
            .then(({data}) => {
                if (data) commit('setMatch', {
                    stage_id: payload.stage_id,
                    match: data.match,
                });
            });
    },
    setMatchScore({dispatch, commit}, payload = {}) {
        let query = (new Query('setMatchScore', 'match').filter({
            match_id: payload.match_id,
            score: payload.score,
            position: payload.position
        })).find([
            'id',
            {team1: ['id', 'name']},
            {team2: ['id', 'name']},
            'score1',
            'score2'
        ]);

        return dispatch('requestGraphql', {type: 'mutation', query: query}, {root: true})
            .then(({data}) => {
                if (data) commit('setMatch', {
                    stage_id: payload.stage_id,
                    match: data.match
                });
            });
    },
};

// mutations
const mutations = {
    setStages(state, {stages}) {
        Vue.set(state, 'stages', stages);
    },
    setStage(state, {stage, position}) {
        Vue.set(state.stages[stage.parent.id], 'child' + position, stage);
        Vue.set(state.stages, stage.id, stage);
    },
    setMatch(state, {stage_id, match}) {
        Vue.set(state.stages[stage_id], 'match', match);
    },
    deleteStage(state, {id, parent_id, position}) {
        Vue.set(state.stages[parent_id], 'child' + position, null);
        Vue.delete(state.stages, id);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
