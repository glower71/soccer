import Query from 'graphql-query-builder-v2';

const state = {
    tournament: {},
};

const actions = {
    getTournament({dispatch, commit}, payload = {}) {
        let query = new Query("tournament").find(['id', 'name']).filter({id: payload.id});
        return dispatch('requestGraphql', {type: 'query', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('setTournament', {tournament: data.tournament});
                }
            });
    },
};

const mutations = {
    setTournament(state, {tournament}) {
        Vue.set(state, 'tournament', tournament);
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
