import Query from 'graphql-query-builder-v2';

const state = {
    stat: {},
};

const getters = {};

// actions
const actions = {
    getStat({dispatch, commit}, payload = {}) {
        let query = new Query('statAggregation', 'stat').filter({
            tournament_id: payload.tournament_id,
        }).find([
            {
                team: ['id', 'name'],
            },
            {
                stat: [
                    {
                        sum: ['is_win', 'is_loss', 'goals_scored', 'goals_passed'],
                    },
                    {
                        max: ['goals_scored', 'goals_passed']
                    }
                ],
            }
        ]);

        return dispatch('requestGraphql', {type: 'query', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('setStat', {stat: data.stat});
                }
            });
    },
};

// mutations
const mutations = {
    setStat(state, {stat}) {
        Vue.set(state, 'stat', stat);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
