import Query from 'graphql-query-builder-v2';

const state = {
    tournaments: {},
};

const actions = {
    createTournament({dispatch, commit}, payload = {}) {
        let query = new Query('createTournament', 'tournament').filter({name: payload.name}).find(['id', 'name']);
        return dispatch('requestGraphql', {query: query, type: 'mutation'}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('addTournament', {tournament: data.tournament});
                }
            });
    },
    getTournaments({dispatch, commit}) {
        let query = new Query('tournaments').find(['id', 'name']);
        return dispatch('requestGraphql', {type: 'query', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('setTournaments', {tournaments: _.keyBy(data.tournaments, 'id')});
                }
            });
    },
    deleteTournament({dispatch, commit}, payload = {}) {
        let query = new Query('deleteTournament', 'tournament').filter({id: payload.id}).find(['id']);
        return dispatch('requestGraphql', {type: 'mutation', query: query}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('deleteTournament', {id: data.tournament.id});
                }
            });
    }
};

const mutations = {
    addTournament(state, {tournament}) {
        Vue.set(state.tournaments, tournament.id, tournament);
    },
    setTournaments(state, {tournaments}) {
        Vue.set(state, 'tournaments', tournaments);
    },
    deleteTournament(state, {id}) {
        Vue.delete(state.tournaments, id);
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
