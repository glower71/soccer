import Query from 'graphql-query-builder-v2';

const state = {
    teams: {},
};

const actions = {
    getTeams({dispatch, commit}) {
        let query = new Query('teams').find(['id', 'name']);
        return dispatch('requestGraphql', {query: query, type: 'query'}, {root: true})
            .then(({data}) => {
                if (data) {
                    commit('setTeams', {teams: _.keyBy(data.teams, 'id')});
                }
            });
    },
    createTeam({dispatch, commit}, payload = {}) {
        let query = new Query('createTeam', 'team').filter({name: payload.name}).find(['id', 'name']);
        return dispatch('requestGraphql', {query: query, type: 'mutation'}, {root: true})
            .then(({data, errors}) => {
                if (data) {
                    commit('addTeam', {team: data.team});
                }
                if (errors) {
                    commit('setErrors', {errors: errors}, {root: true});
                }
            });
    },
};

const mutations = {
    addTeam(state, {team}) {
        Vue.set(state.teams, team.id, team);
    },
    setTeams(state, {teams}) {
        Vue.set(state, 'teams', teams);
    },
    setErrors(state, {errors}) {
        Vue.set(state, 'errors', errors);
    },
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
