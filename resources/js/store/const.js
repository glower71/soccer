import Query from 'graphql-query-builder-v2';

export const gTournament = new Query("tournament").find(['id', 'name']);

export const gTeam = new Query('team').find(['id', 'name']);

export const gTeams = new Query('teams').find(['id', 'name']);

export const gMatch = new Query('match').find([
    'id',
    {team1: ['id', 'name']},
    {team2: ['id', 'name']},
    'score1',
    'score2'
]);;

export const gSetMatchTeam = new Query('setMatchTeam', 'team').find(['id', 'name']);

export const gStages = new Query('stages').find([
        'id',
        {
            parent: ['id']
        }, {
            child1: ['id']
        }, {
            child2: ['id']
        }, gMatch,
        'depth_level'
    ]
);

export const gStage = new Query('stages').find([
        'id',
        {
            parent: ['id']
        }, {
            child1: ['id']
        }, {
            child2: ['id']
        }, gMatch,
        'depth_level'
    ]
);

export const gAddStageChild = new Query('addStageChild', 'stage').find([
        'id',
        {
            parent: ['id']
        }, {
            child1: ['id']
        }, {
            child2: ['id']
        }, gMatch,
        'depth_level'
    ]
);

export const graphQLpath = '/graphql/grid';
