import Vue from 'vue';
import Vuex from 'vuex';
import stages from './modules/stages';
import stat from './modules/stat';
import teams from './modules/teams';
import tournaments from './modules/tournaments';
import tournament from './modules/tournament';
import {graphQLpath} from "./const";
import axios from 'axios';

Vue.use(Vuex);

const state = {
    errors: {},
};

const actions = {
    requestGraphql({getters, commit}, payload) {
        return axios.get(
            graphQLpath,
            {
                params: {query: `${payload.type}{${payload.query}}`},
            },
            {
                headers: {'Content-Type': 'application/graphql'}
            }
        )
            .then(({data}) => {
                if (data.errors) {
                    commit('setErrors', {errors: data.errors});
                }

                return data;
            })
            .catch((error) => {
                commit('setErrors', {errors: {error}});
            });
    },
};

const mutations = {
    setErrors(state, {errors}) {
        Vue.set(state, 'errors', errors);
    },
};

export default new Vuex.Store({
    modules: {
        stages,
        stat,
        teams,
        tournaments,
        tournament,
    },
    actions,
    mutations,
    state
});
