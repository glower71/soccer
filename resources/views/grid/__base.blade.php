<!DOCTYPE html>
<html lang="ru">
<head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script defer src="/js/app.js"></script>
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>
<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/tournaments/">Турниры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/teams/">Команды</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div class="container">
    <div id="app" class="jumbotron">
        @yield('content')
    </div>
</div>

</body>
</html>
