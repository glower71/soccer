<?php namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class TeamType extends GraphQLType
{
    protected $attributes = [
        'name' => 'team',
        'description' => 'team'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the team'
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the team'
            ],
        ];
    }
}
