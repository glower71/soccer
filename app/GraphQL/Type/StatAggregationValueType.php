<?php namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class StatAggregationValueType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Stat Aggregation Value',
        'description' => 'Stat Aggregation Value'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::string(),
                'description' => 'id'
            ],
            'goals_scored' => [
                'type' => Type::string(),
                'description' => 'Goals scored',
            ],
            'goals_passed' => [
                'type' => Type::string(),
                'description' => 'Goals passed',
            ],
            'is_win' => [
                'type' => Type::string(),
                'description' => 'Is winner',
            ],
            'is_loss' => [
                'type' => Type::string(),
                'description' => 'Is looser',
            ],
        ];
    }
}
