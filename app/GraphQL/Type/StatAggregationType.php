<?php namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class StatAggregationType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Stat Aggregation',
        'description' => 'Stat Aggregation'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'team' => [
                'type' => GraphQL::type('team'),
                'description' => 'Team'
            ],
            'stat' => [
                'type' => GraphQL::type('statAggregationGroup'),
                'description' => 'Statistics'
            ],
        ];
    }
}
