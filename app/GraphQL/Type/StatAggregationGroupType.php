<?php namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class StatAggregationGroupType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Stat Aggregation Group',
        'description' => 'Stat Aggregation Group'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'count' => [
                'type' => GraphQL::type('statAggregationValue'),
                'description' => 'Count'
            ],
            'sum' => [
                'type' => GraphQL::type('statAggregationValue'),
                'description' => 'Sum'
            ],
            'avg' => [
                'type' => GraphQL::type('statAggregationValue'),
                'description' => 'Avg'
            ],
            'min' => [
                'type' => GraphQL::type('statAggregationValue'),
                'description' => 'Min'
            ],
            'max' => [
                'type' => GraphQL::type('statAggregationValue'),
                'description' => 'Max'
            ],
        ];
    }
}
