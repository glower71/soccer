<?php namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class StageType extends GraphQLType
{
    protected $attributes = [
        'name' => 'stage',
        'description' => 'stage'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the stage'
            ],
            'tournament' => [
                'description' => 'Tournament',
                'type' => GraphQL::type('tournament')
            ],
            'match' => [
                'description' => 'Match',
                'type' => GraphQL::type('match')
            ],
            'parent' => [
                'description' => 'Parent stage',
                'type' => GraphQL::type('stage')
            ],
            'child1' => [
                'description' => 'Child 1 stage',
                'type' => GraphQL::type('stage')
            ],
            'child2' => [
                'description' => 'Child 2 stage',
                'type' => GraphQL::type('stage')
            ],
            'depth_level' => [
                'description' => 'Depth level',
                'type' => Type::int()
            ],
        ];
    }
}
