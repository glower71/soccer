<?php namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MatchType extends GraphQLType
{
    protected $attributes = [
        'name' => 'match',
        'description' => 'match'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the match',
            ],
            'tournament' => [
                'description' => 'Tournament',
                'type' => GraphQL::type('tournament'),
            ],
            'team1' => [
                'description' => 'Team 1',
                'type' => GraphQL::type('team')
            ],
            'team2' => [
                'description' => 'Team 2',
                'type' => GraphQL::type('team')
            ],
            'score1' => [
                'type' => Type::int(),
                'description' => 'Score 1',
            ],
            'score2' => [
                'type' => Type::int(),
                'description' => 'Score 2',
            ],
            'winner' => [
                'description' => 'Team',
                'type' => GraphQL::type('team')
            ],
            'is_finished' => [
                'description' => 'Is finished',
                'type' => Type::boolean(),
            ],
        ];
    }
}
