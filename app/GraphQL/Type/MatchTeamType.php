<?php namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MatchTeamType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Match team',
        'description' => 'Match team'
    ];

    /*
    * Uncomment following line to make the type input object.
    * http://graphql.org/learn/schema/#input-types
    */
    // protected $inputObject = true;

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the stage'
            ],
            'match' => [
                'description' => 'Tournament',
                'type' => GraphQL::type('match')
            ],
            'team' => [
                'description' => 'Team',
                'type' => GraphQL::type('team')
            ],
            'goals_scored' => [
                'description' => 'Goals scored',
                'type' => Type::int()
            ],
            'goals_passed' => [
                'description' => 'Goals passed',
                'type' => Type::int()
            ],
            'is_win' => [
                'description' => 'Is win',
                'type' => Type::boolean()
            ],
            'is_loss' => [
                'description' => 'Is loss',
                'type' => Type::boolean()
            ],
            'position' => [
                'description' => 'Position',
                'type' => Type::int()
            ],
        ];
    }
}
