<?php namespace App\GraphQL\Query;

use App\Grid\Models\Stage;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class DeleteStageMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Delete Stage'
    ];

    public function type(): Type
    {
        return GraphQL::type('stage');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $stage = Stage::findOrFail($args['id']);
        if($stage->child1 != null || $stage->child2 != null) {
            throw new \Exception('Stage has children!');
        }

        $stage->delete();
        return $stage;
    }
}
