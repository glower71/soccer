<?php namespace App\GraphQL\Query;

use App\Grid\Models\Stage;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\DB;

class AddStageChildMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Add Child Stage'
    ];

    public function type(): Type
    {
        return GraphQL::type('stage');
    }

    public function args(): array
    {
        return [
            'parent_id' => [
                'name' => 'parent_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
            'position' => [
                'name' => 'position',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'between:1,2']
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $parent = Stage::find($args['parent_id']);

        if (!$parent) {
            throw new \Exception('Parent stage not found!');
        }

        DB::beginTransaction();

        $child = $parent->addChild($args['position']);

        DB::commit();

        return $child;
    }
}
