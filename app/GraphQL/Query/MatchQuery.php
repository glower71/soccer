<?php namespace App\GraphQL\Query;

use App\Grid\Models\Match;
use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class MatchQuery extends Query
{
    protected $attributes = [
        'name' => 'match'
    ];

    public function type(): Type
    {
        return GraphQL::type('match');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        return Match::find($args['id']);
    }
}
