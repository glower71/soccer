<?php namespace App\GraphQL\Query;

use App\Grid\Models\Match;
use App\Grid\Models\Stage;
use App\Grid\Models\Team;
use Exception;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\DB;

class SetMatchScoreMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Set match team mutation'
    ];

    public function type(): Type
    {
        return GraphQL::type('match');
    }

    public function args(): array
    {
        return [
            'match_id' => [
                'name' => 'match_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
            'score' => [
                'name' => 'score',
                'type' => Type::int(),
                'rules' => ['required'],
            ],
            'position' => [
                'name' => 'position',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'between:1,2'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $match = Match::find($args['match_id']);
        if (!$match) {
            throw new Exception('Match not found!');
        }

        DB::beginTransaction();

        $match->{'score' . $args['position']} = $args['score'];
        $match->save();

        DB::commit();

        return $match;
    }
}
