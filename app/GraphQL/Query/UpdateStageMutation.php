<?php namespace App\GraphQL\Query;

use App\Grid\Models\Stage;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class UpdateStageMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Update Stage'
    ];

    public function type(): Type
    {
        return GraphQL::type('stage');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1']
            ],
            'team1_id' => [
                'name' => 'team1_id',
                'type' => Type::int()
            ],
            'team2_id' => [
                'name' => 'team2_id',
                'type' => Type::int()
            ],
            'team1_scored' => [
                'name' => 'team1_scored',
                'type' => Type::int()
            ],
            'team2_scored' => [
                'name' => 'team2_scored',
                'type' => Type::int()
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $stage = Stage::findOrFail($args['id']);

        if(isset($args['team1_id'])) {
            $stage->team1_id = $args['team1_id'];
        }

        if(isset($args['team2_id'])) {
            $stage->team2_id = $args['team2_id'];
        }

        if(isset($args['team1_scored'])) {
            $stage->team1_scored = $args['team1_scored'];
        }

        if(isset($args['team2_scored'])) {
            $stage->team2_scored = $args['team2_scored'];
        }

        $stage->save();

        return $stage;
    }
}
