<?php namespace App\GraphQL\Query;

use App\Grid\Models\MatchTeam;
use Closure;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\DB;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;

class StatAggregationQuery extends Query
{
    protected $attributes = [
        'name' => 'Stat Aggregation'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('statAggregation'));
    }

    public function args(): array
    {
        return [
            'tournament_id' => [
                'name' => 'tournament_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1']
            ],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info, Closure $getSelectFields)
    {
        $query = MatchTeam::query()
            ->where('tournament_id', $args['tournament_id'])
            ->addSelect('team_id')      //is needed for loading team
            ->whereNotNull('team_id')
            ->groupBy('team_id')
            ->orderBy('team_id', 'asc');

        $asked = $info->getFieldSelection(3);

        if (isset($asked['stat'])) {
            foreach ($asked['stat'] as $func => $fields) {
                foreach ($fields as $field => $isAsked) {
                    $query->addSelect(DB::raw($func . '(' . $field . ') as ' . $func . '_' . $field));
                }
            }
        }

        if (isset($asked['team'])) {
            $query->with('team');
        }

        $values = $query->get();

        $result = [];
        foreach ($values as $item) {
            $resItem = [];

            if (isset($asked['team'])) {
                $team = [];
                foreach ($asked['team'] as $teamKey => $teamValue) {
                    $team[$teamKey] = $item->team->{$teamKey};
                }

                $resItem['team'] = $team;
            }

            if (isset($asked['stat'])) {
                foreach ($asked['stat'] as $func => $fields) {
                    foreach ($fields as $field => $isAsked) {
                        $resItem['stat'][$func][$field] = $item->{$func . '_' . $field};
                    }
                }
            }
            $result[] = $resItem;
        }

        return $result;
    }
}
