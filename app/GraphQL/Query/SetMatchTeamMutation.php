<?php namespace App\GraphQL\Query;

use App\Grid\Models\Match;
use App\Grid\Models\Stage;
use App\Grid\Models\Team;
use Exception;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class SetMatchTeamMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Set match team mutation'
    ];

    public function type(): Type
    {
        return GraphQL::type('match');
    }

    public function args(): array
    {
        return [
            'match_id' => [
                'name' => 'match_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
            'team_id' => [
                'name' => 'team_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
            'position' => [
                'name' => 'position',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'between:1,2']
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $match = Match::find($args['match_id']);
        if (!$match) {
            throw new Exception('Match not found!');
        }

        $team = Team::find($args['team_id']);
        if (!$team) {
            throw new Exception('Team not found!');
        }

        try {
            $match->{'team' . $args['position']}()->associate($team)->save();
            return $match;
        } catch (Exception $exception) {
            throw $exception;
        }
    }
}
