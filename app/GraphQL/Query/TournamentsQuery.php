<?php namespace App\GraphQL\Query;

use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class TournamentsQuery extends Query
{
    protected $attributes = [
        'name' => 'Tournaments'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('tournament'));
    }

    public function args(): array
    {
        return [

        ];
    }

    public function resolve($root, $args)
    {
        return Tournament::all();
    }
}
