<?php namespace App\GraphQL\Query;

use App\Grid\Models\Stage;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class StagesQuery extends Query
{
    protected $attributes = [
        'name' => 'Stages'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('stage'));
    }

    public function args(): array
    {
        return [
            'tournament_id' => [
                'name' => 'tournament_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1']
            ],
        ];
    }

    public function resolve($root, $args)
    {
        return Stage::where('tournament_id', $args['tournament_id'])->get();
    }
}
