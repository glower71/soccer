<?php namespace App\GraphQL\Query;

use App\Grid\Emulator;
use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class EmulateTournamentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Emulate Tournament'
    ];

    public function type(): Type
    {
        return GraphQL::type('tournament');
    }

    public function args(): array
    {
        return [
            'tournament_id' => [
                'name' => 'tournament_id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
            'depth' => [
                'name' => 'depth',
                'type' => Type::int(),
                'rules' => ['required'],
            ],
            'play' => [
                'name' => 'play',
                'type' => Type::boolean(),
                'default' => true,
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $tournament = Tournament::find($args['tournament_id']);
        if (!$tournament) {
            throw new \Exception('Tournament not found!');
        }

        $tournament = Emulator::emulate($tournament, $args['depth'], $args['play']);
        return $tournament->stages;
    }
}
