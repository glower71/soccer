<?php namespace App\GraphQL\Query;

use App\Grid\Models\Stage;
use App\Grid\Models\Team;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class CreateTeamMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Create team'
    ];

    public function type(): Type
    {
        return GraphQL::type('team');
    }

    public function args(): array
    {
        return [
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $team = new Team([
            'name' => $args['name']
        ]);

        $team->save();
        return $team;
    }
}
