<?php namespace App\GraphQL\Query;

use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class UpdateTournamentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Update Tournament'
    ];

    public function type(): Type
    {
        return GraphQL::type('tournament');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1']
            ],
            'name' => [
                'name' => 'name',
                'type' => Type::string()
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $tournament = Tournament::findOrFail($args['id']);

        if(isset($args['name'])) {
            $tournament->name = $args['name'];
        }

        $tournament->save();

        return $tournament;
    }
}
