<?php namespace App\GraphQL\Query;

use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;

class DeleteTournamentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Delete Tournament'
    ];

    public function type(): Type
    {
        return GraphQL::type('tournament');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        $tournament = Tournament::findOrFail($args['id']);
        $tournament->delete();
        return $tournament;
    }
}
