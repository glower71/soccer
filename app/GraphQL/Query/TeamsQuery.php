<?php namespace App\GraphQL\Query;

use App\Grid\Models\Team;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class TeamsQuery extends Query
{
    protected $attributes = [
        'name' => 'teams'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('team'));
    }

    public function resolve($root, $args)
    {
        return Team::all();
    }
}
