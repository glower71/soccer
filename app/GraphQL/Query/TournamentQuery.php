<?php namespace App\GraphQL\Query;

use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class TournamentQuery extends Query
{
    protected $attributes = [
        'name' => 'Tournament'
    ];

    public function type(): Type
    {
        return GraphQL::type('tournament');
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::int(),
                'rules' => ['required', 'integer', 'min:1']
            ],
        ];
    }

    public function resolve($root, $args)
    {
        return Tournament::find($args['id']);
    }
}
