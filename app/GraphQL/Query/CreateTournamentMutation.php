<?php namespace App\GraphQL\Query;

use App\Grid\Models\Tournament;
use GraphQL;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\DB;

class CreateTournamentMutation extends Mutation
{
    protected $attributes = [
        'name' => 'Create Tournament'
    ];

    public function type(): Type
    {
        return GraphQL::type('tournament');
    }

    public function args(): array
    {
        return [
            'name' => [
                'name' => 'name',
                'type' => Type::string(),
                'rules' => ['required'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        DB::beginTransaction();

        $tournament = new Tournament([
            'name' => $args['name'],
        ]);
        $tournament->save();

        DB::commit();
        return $tournament;
    }
}
