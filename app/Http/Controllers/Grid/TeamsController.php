<?php namespace App\Http\Controllers\Grid;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class TreeController
 * @package App\Http\Controllers
 */
class TeamsController extends Controller
{
    public function list(Request $request)
    {
        return view('grid.teams');
    }

    public function detail(Request $request)
    {
        return view('grid.team');
    }
}
