<?php namespace App\Http\Controllers\Grid;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class TreeController
 * @package App\Http\Controllers
 */
class TournamentsController extends Controller
{
    public function list(Request $request)
    {
        return view('grid.tournaments');
    }

    public function detail($id)
    {
        return view('grid.tournament', ['id' => $id]);
    }
}
