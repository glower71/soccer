<?php namespace App\Http\Controllers\Grid;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class TreeController
 * @package App\Http\Controllers
 */
class StatController extends Controller
{
    public function detail($id)
    {
        return view('grid.stat', ['id' => $id]);
    }
}
