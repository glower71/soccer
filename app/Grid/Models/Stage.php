<?php

namespace App\Grid\Models;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property Collection competitors
 * @property Collection matches
 * @property mixed team1_id
 * @property mixed team2_id
 * @property mixed team1_goals_scored
 * @property mixed team2_goals_scored
 * @property mixed parent_id
 * @property Stage child1
 * @property Stage child2
 * @property mixed is_finished
 * @property Match match
 * @property Stage parent
 * @property Tournament tournament
 * @property int depth_level
 */
class Stage extends Model
{
    protected $fillable = [
        'name',
        'tournament_id',
        'match_id',
        'parent_id',
        'child1_id',
        'child2_id',
    ];

    /**
     * @return BelongsTo
     */
    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    /**
     * @return BelongsTo
     */
    public function match()
    {
        return $this->belongsTo(Match::class);
    }

    /**
     * @return BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Stage::class);
    }

    /**
     * @return BelongsTo
     */
    public function child1()
    {
        return $this->belongsTo(Stage::class);
    }

    /**
     * @return BelongsTo
     */
    public function child2()
    {
        return $this->belongsTo(Stage::class);
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();
        static::created(function (Stage $stage) {
            $match = new Match();
            $match->tournament()->associate($stage->tournament);
            $match->save();

            $stage->match()->associate($match);
            $stage->save();
        });
    }

    /**
     * @return int
     */
    public function getDepthLevelAttribute()
    {
        if ($this->parent == null) {
            return 1;
        }

        return $this->parent->depth_level + 1;
    }

    /**
     * @param int $position
     * @return Stage
     */
    public function addChild(int $position)
    {
        $child = new Stage();
        $child->tournament()->associate($this->tournament)->save();

        $this->{'child' . $position}()->associate($child);
        $this->save();
        $child->parent()->associate($this);
        $child->save();

        return $child;
    }

    /**
     * @throws Exception
     */
    public function emulate()
    {
        $team1_score = random_int(0, 5);
        do {
            $team2_score = random_int(0, 5);
        } while ($team1_score == $team2_score);

        $this->match->play($team1_score, $team2_score);

        if ($this->parent != null) {
            if ($this->parent->child1 != null) {
                if ($this->parent->child1->id == $this->id) {
                    $this->parent->match->team1()->associate($this->match->winner)->save();
                }
            }

            if ($this->parent->child2 != null) {
                if ($this->parent->child2->id == $this->id) {
                    $this->parent->match->team2()->associate($this->match->winner)->save();
                }
            }

            $this->parent->save();
        }
    }
}
