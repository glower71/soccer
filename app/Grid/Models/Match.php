<?php

namespace App\Grid\Models;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property Collection matchTeams
 * @property boolean is_finished
 * @property MatchTeam matchTeam1
 * @property MatchTeam matchTeam2
 * @property Team winner
 * @property int score1
 * @property int score2
 * @property int tournament_id
 * @property int team1_id
 * @property int team2_id
 */
class Match extends Model
{
    protected $fillable = [
        'tournament_id',
        'team1_id',
        'team2_id',
        'score1',
        'score2',
        'is_finished',
    ];

    /**
     * @return BelongsTo
     */
    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    /**
     * @return BelongsTo
     */
    public function team1()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * @return BelongsTo
     */
    public function team2()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * @return HasMany
     */
    public function matchTeams()
    {
        return $this->hasMany(MatchTeam::class)->orderBy('position', 'asc');
    }

    /**
     * @return MatchTeam
     */
    public function getMatchTeam1Attribute()
    {
        return $this->matchTeams->keyBy('position')[1];
    }

    /**
     * @return MatchTeam
     */
    public function getMatchTeam2Attribute()
    {
        return $this->matchTeams->keyBy('position')[2];
    }

    /**
     * @return mixed
     */
    public function getWinnerAttribute()
    {
        if ($this->matchTeams()->whereIsWin(true)->get()->first() != null) {
            return $this->matchTeams()->whereIsWin(true)->get()->first()->team;
        }

        return false;
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();
        static::created(function (Match $match) {
            for ($i = 1; $i <= 2; $i++) {
                $matchTeam = new MatchTeam([
                    'tournament_id' => $match->tournament_id,
                    'position' => $i
                ]);

                $matchTeam->match()->associate($match)->save();
            }
        });

        static::updating(function (Match $match) {
            if ($match->isDirty('team1_id')) {
                $match->matchTeam1->team_id = $match->team1_id;
            }

            if ($match->isDirty('team2_id')) {
                $match->matchTeam2->team_id = $match->team2_id;
            }

            if ($match->isDirty('score1')) {
                $match->matchTeam1->goals_scored = $match->score1;
                $match->matchTeam2->goals_passed = $match->score1;
            }

            if ($match->isDirty('score2')) {
                $match->matchTeam2->goals_scored = $match->score2;
                $match->matchTeam1->goals_passed = $match->score2;
            }

            if ($match->isDirty('score1') || $match->isDirty('score2')) {
                if ($match->matchTeam1->goals_scored !== null && $match->matchTeam2->goals_scored !== null) {
                    if ($match->matchTeam1->goals_scored > $match->matchTeam2->goals_scored) {
                        $match->matchTeam1->is_win = true;
                        $match->matchTeam1->is_loss = false;

                        $match->matchTeam2->is_win = false;
                        $match->matchTeam2->is_loss = true;
                    }

                    if ($match->matchTeam1->goals_scored < $match->matchTeam2->goals_scored) {
                        $match->matchTeam1->is_win = false;
                        $match->matchTeam1->is_loss = true;

                        $match->matchTeam2->is_win = true;
                        $match->matchTeam2->is_loss = false;
                    }

                    $match->matchTeam1->is_finished = true;
                    $match->matchTeam2->is_finished = true;
                    $match->is_finished = true;
                } else {
                    $match->matchTeam1->is_win = null;
                    $match->matchTeam1->is_loss = null;

                    $match->matchTeam2->is_win = null;
                    $match->matchTeam2->is_loss = null;

                    $match->matchTeam1->is_finished = false;
                    $match->matchTeam2->is_finished = false;
                    $match->is_finished = false;
                }
            }

            $match->matchTeam1->save();
            $match->matchTeam2->save();
        });
    }

    /**
     * @param $team1_score
     * @param $team2_score
     * @throws Exception
     */
    public function play($team1_score, $team2_score)
    {
        if (!$this->is_finished) {
            if (!$this->team1_id) {
                throw new Exception('team 1 is undefined');
            }

            if (!$this->team2_id) {
                throw new Exception('team 2 is undefined');
            }

            $this->score1 = $team1_score;
            $this->score2 = $team2_score;

            $this->save();
        } else {
            throw new Exception('Match is already played!');
        }
    }
}
