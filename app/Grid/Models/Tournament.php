<?php

namespace App\Grid\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

/**
 * @property Collection competitors
 * @property Collection matches
 * @property Collection stages
 */
class Tournament extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * @return HasMany
     */
    public function stages()
    {
        return $this->hasMany(Stage::class);
    }

    /**
     * @return Stage
     */
    public function getRootAttribute()
    {
        return $this->stages()->whereNull('parent_id')->get()->first();
    }

    /**
     *
     */
    public static function boot()
    {
        parent::boot();
        static::created(function ($tournament) {
            $stage = new Stage();
            $stage->tournament()->associate($tournament);
            $stage->save();
        });
    }
}
