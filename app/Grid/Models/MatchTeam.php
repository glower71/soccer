<?php

namespace App\Grid\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Match match
 * @property int goals_scored
 * @property int goals_passed
 * @property boolean is_win
 * @property boolean is_loss
 * @property int position
 * @property int team_id
 * @property bool is_finished
 */
class MatchTeam extends Model
{
    protected $fillable = [
        'tournament_id',
        'match_id',
        'team_id',
        'goals_scored',
        'goals_passed',
        'is_win',
        'is_loss',
        'position',
    ];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function match()
    {
        return $this->belongsTo(Match::class);
    }
}
