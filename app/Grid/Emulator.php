<?php namespace App\Grid;

use App\Grid\Models\Stage;
use App\Grid\Models\Team;
use App\Grid\Models\Tournament;
use Exception;
use Illuminate\Database\Eloquent\Collection;

class Emulator
{
    /**
     * @var Tournament
     */
    protected $tournament;
    /**
     * @var Collection
     */
    protected $teams;
    /**
     * @var int
     */
    protected $depth;

    /**
     * Emulator constructor.
     * @param Tournament $tournament
     * @param int $depth
     * @param bool $play
     */
    public function __construct(Tournament $tournament, int $depth)
    {
        $this->tournament = $tournament;
        $this->depth = $depth;
    }

    /**
     * @return Tournament
     * @throws Exception
     */
    public function run()
    {
        $this->teams = Team::all()->shuffle();

        if ($this->teams->count() < pow(2, $this->depth)) {
            throw new Exception('No enough teams for emulation!');
        }

        self::processStage($this->tournament->stages[0], 1);
        return $this->tournament;
    }

    /**
     * @param Stage $stage
     * @param int $curDepth
     * @throws Exception
     */
    protected function processStage(Stage $stage, int $curDepth)
    {
        if ($curDepth < $this->depth) {
            $child1 = $stage->addChild(1);
            self::processStage($child1, $curDepth + 1);

            $child2 = $stage->addChild(2);
            self::processStage($child2, $curDepth + 1);
        } else {
            $stage->match->team1()->associate($this->teams->shift());
            $stage->match->team2()->associate($this->teams->shift());

            $stage->match->save();
        }

        $stage->emulate();
    }
}
