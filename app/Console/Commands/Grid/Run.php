<?php namespace App\Console\Commands\Grid;

use App\Grid\Emulator;
use App\Grid\Models\Tournament;
use Exception;
use Illuminate\Console\Command;


class Run extends Command
{
    /**
     * The name and signature of the console command
     *
     * @var string
     */
    protected $signature = 'grid:run';
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'Run tournament';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Run tournament";

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        $name = $this->ask('Tournament name?');
        $depthLevel = $this->choice("Depth level", [1, 2, 3, 4], '4');

        $tournament = Tournament::create(['name' => $name]);
        $emulator = new Emulator($tournament, $depthLevel);
        $emulator->run();
    }
}
