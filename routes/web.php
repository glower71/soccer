<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::namespace('Grid')->group(function () {
    Route::get('/tournaments', 'TournamentsController@list');
    Route::get('/tournaments/{id}', 'TournamentsController@detail');
    Route::get('/stat/{id}', 'StatController@detail');
    Route::get('/teams', 'TeamsController@list');
});
